/* Login เข้าสู่ระบบ */
// แสดงผู้ใช้งานที่จดจำไว้
$('.account_info').on("click", function() {
    var key = $(this).data("key");
    var imgs = $(this).data("imgs");
    var username = $(this).data("username");
    if ($('.card-user-choose').hasClass("d-none")) {
        $('.card-user-choose').removeClass("d-none");
    }
    $('.card-user-choose').addClass("d-block");
    //แสดงชื่อ user
    $('.card-user-choose p').html(username);
    //แสดงรูป IMG
    if (imgs != "") {
        $('#redmeimgs').attr('src', imgs);
    } else {
        $('#redmeimgs').attr('src', 'assets/img/blankprofile.png');
    }
    $('#redmeimgs').attr('alt', key);

    $('.card-user-cookie').addClass("d-none");
    if ($('.card-user-cookie').hasClass("d-inline-block")) {
        $('.card-user-cookie').removeClass("d-inline-block");
    }

    $('div.username').addClass("d-none");
    if ($('div.username').hasClass("d-block")) {
        $('div.username').removeClass("d-block");
    }


    $('#username').val(username);

    $('button.close').attr("data-key", key);

    console.log("KEY " + key);
    console.log("imgs " + imgs);

});

$('.notme').on("click", function() {
    $('.card-user-choose').addClass("d-none");
    $('.card-user-choose').removeClass("d-block");
    if ($('div.username').hasClass("d-none")) {
        $('div.username').removeClass("d-none");
    }
    $('div.username').addClass("d-block");
    $('.card-user-cookie').addClass("d-inline-block");
    if ($('.card-user-cookie').hasClass("d-none")) {
        $('.card-user-cookie').removeClass("d-none");
    }
    $("form")[0].reset();
})

$('#deleteModal').on('show.bs.modal', function(event) {
    var button = $(event.relatedTarget);
    var key = button.data('key');
    $('.btn-delete').on("click", function() {
        $.ajax({
            url: "ajax_function.php?obj=delete_account_cookie",
            method: "POST",
            data: { key: key },
        }).done(function(result) {
            if (result == "success") {
                $('#deleteModal').modal('hide');
                $('.card-user-' + key).addClass("d-none");
                $('.card-user-' + key).remove();
                $('.card-user-choose').addClass("d-none");
                if ($('.card-user-choose').hasClass("d-block")) {
                    $('.card-user-choose').removeClass("d-block");
                }

                if ($('div.username').hasClass("d-none")) {
                    $('div.username').removeClass("d-none");
                }
                if ($('.card-user-cookie').hasClass("d-none")) {
                    $('.card-user-cookie').removeClass("d-none");
                }
                $('div.username').addClass("d-block");
                $('.card-user-cookie').addClass("d-inline-block");
                $("form")[0].reset();
            }
        });
    })
    var modal = $(this);
    // modal.find('.account_id').text('account ' + key);
    // modal.find('.modal-body input').val(recipient)
});

function check_username() {
    var username = $('#username').val();
    if (username == "" || username.length <= 0) {
        $('#username').addClass('is-invalid');
        $('#error-username').text('กรุณากรอกชื่อผู้ใช้งาน');
    } else {
        if ($('#username').hasClass('is-invalid')) {
            $('#username').removeClass('is-invalid');
            $('#error-username').text('');
        }
    }
}

function check_password() {
    var password = $('#password').val();
    if (password == "" || password.length <= 0) {
        $('#password').addClass('is-invalid');
        $('#error-password').text('กรุณากรอกรหัสผ่าน');
    } else if (password.length < 6) {
        $('#password').addClass('is-invalid');
        $('#error-password').text('ป้อนรหัสผ่านอย่างน้อย 6 ตัวอักษร!');
    } else {
        if ($('#password').hasClass('is-invalid')) {
            $('#password').removeClass('is-invalid');
            $('#error-password').text('');
        }
    }
}

$('#username').on("keyup blur keypress", function() {
    check_username();
});

$('#password').on("keyup blur keypress", function() {
    check_password();
});

$('#frmLogin').on("submit", function(e) {
    e.preventDefault();
    var username = $('#username').val();
    var frmdata = $("#frmLogin").serialize();
    var password = $('#password').val();
    check_username();
    check_password();



    if ((username != '' && username.length > 0) && (password != '' && password.length >= 6)) {
        /*start ajax*/
        $.ajax({
            url: 'ajax_function.php?obj=check_login',
            type: "POST",
            data: frmdata,
            beforeSend: function() {
                $('.btn-login').css("display", "none");
                $('.btn-loading').removeClass("d-none");
            }
        }).done(function(result, textStatus, xhr) {
            if (result == "success") {
                window.location.href = "index.php";
            } else {
                $('.alert-incorrect').removeClass('d-none').text('ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง');
                $('#username').addClass('is-invalid');
                $('#password').addClass('is-invalid');
                $("#frmLogin")[0].reset();

                $('.btn-login').css("display", "block");
                $('.btn-loading').addClass("d-none");
            }
        });
        /*end ajax*/
    } else {
        console.log("กรุณากรอกข้อมูลให้ถูกต้อง (Err001)");
    }
})

/* Register สมัครสมาชิก */

// user ชื่อผู้ใช้
var checkUserDB = new Boolean(false);

function check_regUsername() {
    var username = $('#regUsername').val();
    if (username == "" || username.length <= 0) {
        $('#regUsername').addClass('is-invalid');
        $('#error-regUsername').text('กรุณากรอกชื่อผู้ใช้');
    } else if (username.length < 6) {
        $('#regUsername').addClass('is-invalid');
        $('#error-regUsername').text('ป้อนชื่อผู้ใช้อย่างน้อย 6 ตัวอักษร!');
    } else {

        setTimeout(function() {
            var frmdata = $("#frmRegister").serialize();
            /*start ajax*/
            $.ajax({
                url: 'ajax_function.php?obj=check_regUsername',
                type: "POST",
                data: frmdata,
                beforeSend: function() {

                }
            }).done(function(result, textStatus, xhr) {
                console.log("result " + result);

                if (result == "success") {
                    if ($('#regUsername').hasClass('is-invalid')) {
                        $('#regUsername').removeClass('is-invalid');
                        $('#error-regUsername').text('');
                        checkUserDB = true;
                    }
                } else {
                    $('#regUsername').addClass('is-invalid');
                    $('#error-regUsername').text('ชื่อผู้ใช้นี้ถูกใช้ไปแล้ว!');
                    checkUserDB = false;
                }
            });
            /*end ajax*/
        }, 500); // 3000 = 1s, 1วิ

    }
}
$('#regUsername').on("keyup blur keypress", function() {
    check_regUsername();
});
// password รหัสผ่าน
function check_regPassword() {
    var password = $('#regPassword').val();
    if (password == "" || password.length <= 0) {
        $('#regPassword').addClass('is-invalid');
        $('#error-regPassword').text('กรุณากรอกรหัสผ่าน');
    } else if (password.length < 6) {
        $('#regPassword').addClass('is-invalid');
        $('#error-regPassword').text('ป้อนรหัสผ่านอย่างน้อย 6 ตัวอักษร!');
    } else {
        if ($('#regPassword').hasClass('is-invalid')) {
            $('#regPassword').removeClass('is-invalid');
            $('#error-regPassword').text('');
        }
    }


}
$('#regPassword').on("keyup blur keypress", function() {
    check_regPassword();
});
// AgainPassworad ยืนยันรหัสผ่าน
function check_regAgainPassworad() {
    var password = $('#regPassword').val();
    var againpassword = $('#regAgainPassworad').val();
    if (password == againpassword && againpassword != "" && againpassword.length >= 6) {
        $('#regAgainPassworad').removeClass('is-invalid');
        $('#error-regAgainPassworad').text('');
    } else {
        $('#regAgainPassworad').addClass('is-invalid');
        $('#error-regAgainPassworad').text('รหัสผ่านไม่ตรงกัน!');
    }
}

$('#regAgainPassworad').on("keyup blur keypress", function() {
    check_regAgainPassworad();
});

function check_regUserDB() {
    var frmdata = $("#frmRegister").serialize();
    /*start ajax*/
    $.ajax({
        url: 'ajax_function.php?obj=check_regUsername',
        type: "POST",
        data: frmdata,
        beforeSend: function() {

        }
    }).done(function(result, textStatus, xhr) {
        console.log("result " + result);

        if (result == "success") {
            if ($('#regUsername').hasClass('is-invalid')) {
                $('#regUsername').removeClass('is-invalid');
                $('#error-regUsername').text('');
            }
        } else {
            $('#regUsername').addClass('is-invalid');
            $('#error-regUsername').text('ชื่อผู้ใช้นี้ถูกใช้ไปแล้ว!');
        }
    });
    /*end ajax*/
}


$('#frmRegister').on("submit", function(e) {
    e.preventDefault();
    var username = $('#regUsername').val();
    var frmdata = $("#frmRegister").serialize();
    var password = $('#regPassword').val();
    var againpassword = $('#regAgainPassworad').val();

    check_regUsername();
    check_regUserDB();
    check_regPassword();
    check_regAgainPassworad();

    if ((username != '' && username.length > 0) && (password != '' && password.length > 0) && (password == againpassword && againpassword != "" && againpassword.length >= 6) && checkUserDB) {
        /*start ajax*/
        $.ajax({
            url: 'ajax_function.php?obj=register',
            type: "POST",
            data: frmdata,
            beforeSend: function() {
                $('.btn-register').css("display", "none");
                $('.btn-loading').removeClass("d-none");
            }
        }).done(function(result, textStatus, xhr) {
            if (result == "success") {
                window.location.href = "index.php";
            } else {
                $('.alert-incorrect').removeClass('d-none').text('สมัครสมาชิกล้มเหลว (Err003)');
                $('#username').addClass('is-invalid');
                $('#password').addClass('is-invalid');
                $("#frmRegister")[0].reset();

                $('.btn-register').css("display", "block");
                $('.btn-loading').addClass("d-none");
                // setTimeout(function() { $('.alert-incorrect').alert('close'); }, 3000);
            }
        });
        /*end ajax*/
    } else {
        console.log("กรุณากรอกข้อมูลให้ถูกต้อง (Err002)");
    }
})