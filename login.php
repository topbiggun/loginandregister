<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Dev'NokBan</title>
  
  <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css"> 
  <link rel="stylesheet" href="node_modules/@fortawesome/fontawesome-free/css/all.css">  
  <link rel="stylesheet" href="include/libs/css/login.css">
  <link rel="stylesheet" href="include/libs/css/footer.css">
  <style>
    /* @media only screen and (max-width: 1200px) {
      #BodyLogin {
        background-color: lightblue;

      }
    } */

    
  </style>

</head>

<!-- body {
  background-color: #e9ecef; -->
<!-- background-color: #4e73df; -->
<!-- background-image: -webkit-gradient(linear, left top, left bottom, color-stop(10%, #4e73df), to(#224abe)); -->
<!-- background-image: linear-gradient(180deg, #4e73df 10%, #224abe 100%); -->
<!-- background-size: cover; -->
<!-- } -->

<body>

  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Dev'Nok<span class="fa fa-home"></span></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <!-- <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Link</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Dropdown
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Something else here</a>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
        </li>
      </ul>
      <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
    </div> -->
  </nav>
  <!-- EndNavbar -->

  <!-- Body -->
  <div class="container" id="BodyLogin">
    <div class="row">
      <!-- col-12 col-md-9 col-xl-8 py-md-3 pl-md-5 bd-content -->
      <!-- ซ่อน div class="d-none d-md-block" -->
      <div class="col-12 col-md-7 col-xl-7 py-md-3 pl-md-5 bd-content d-none d-md-block" style="/*background-color: red;*/">
        
        <h1>ซ้าย</h1>
      </div>
      <div class="col-12 col-md-5 col-xl-5 py-md-3 /*pl-md-5*/ bd-content" style="/*background-color: blue;*/">

        <div class="container mt-0 pt-0">
          <div class="card mx-auto border-0">
            <div class="card-header border-bottom-0 bg-transparent">
              <ul class="nav nav-tabs justify-content-center pt-4" id="pills-tab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active text-primary" id="pills-login-tab" data-toggle="pill" href="#pills-login" role="tab" aria-controls="pills-login" aria-selected="true">
                    <h6>เข้าสู่ระบบ</h6>
                  </a>
                </li>

                <li class="nav-item">
                  <a class="nav-link text-primary" id="pills-register-tab" data-toggle="pill" href="#pills-register" role="tab" aria-controls="pills-register" aria-selected="false">
                    <h6>สมัครสมาชิก</h6>
                  </a>
                </li>
              </ul>
            </div>

            <div class="card-body pb-4">
              <div class="tab-content" id="pills-tabContent">
                <!-- เข้าสู่ระบบ Login -->
                <div class="tab-pane fade show active" id="pills-login" role="tabpanel" aria-labelledby="pills-login-tab">
                  <form id="frmLogin" name="frmLogin" action="#" method="POST" autocomplete="off">
                    <div class="alert alert-danger alert-incorrect d-none" role="alert"></div>
                    <?php if (isset($_COOKIE["user_id"])) { ?>
                      <?php foreach ($_COOKIE["user_id"] as $k => $v) { ?>
                        <div title="<?php echo $v; ?>" style="width:99px;display:inline-block;position:relative;cursor:pointer;" class="text-center card-user-cookie card-user-<?php echo $k; ?> formlogin" data-key="<?php echo $k; ?>" data-username="<?php echo $v; ?>">
                          <button type="button" class="close" aria-label="Close" style="position: absolute; right: 14px;" data-toggle="modal" data-target="#deleteModal" data-key="<?php echo $k; ?>" data-username="<?php echo $v; ?>">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          <div class="account_info" data-key="<?php echo $k; ?>" data-username="<?php echo $k; ?>" data-imgs="<?php echo $v; ?>">
                            <img src="<?php echo $v ?>" alt="" width="75"  style="width:75px ;height:75px ;" class="img-fluid rounded-circle img-thumbnail">
                            <?php echo "<p>" . $k . "</p>"; ?>
                          </div>
                        </div>
                      <?php } ?>
                    <?php } ?>

                    <div title="" style="width:150px;display:inline-block;position:relative;cursor:pointer;" class="text-center  formlogin card-user-choose d-none mx-auto">
                      <button type="button" class="close" aria-label="Close" style="position: absolute; right: 14px;" data-toggle="modal" data-target="#deleteModal">
                        <span aria-hidden="true">&times;</span>
                      </button>
                      <div class="account_info_choose">
                        <img id="redmeimgs" src="https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_640.png" alt="" width="75" style="width:75px ;height:75px ;"  class="img-fluid rounded-circle img-thumbnail">
                      </div>
                      <p style="display:inline-block;margin: 0;"></p> <span class="small notme text-danger">ไม่ใช่ฉัน?</span>
                    </div>

                    <div class="form-group username">
                      <div class="input-group">
                        <input type="text" class="form-control login-input" id="username" name="username" placeholder="ชื่อผู้ใช้" value="">
                        <div class="input-group-prepend">
                          <div class="input-group-text login-input-group-text" id="btnGroupAddon"><span class="fa fa-user"></span>
                          </div>
                        </div>
                      </div>
                      <span id="error-username" class="text-danger small"></span>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <input type="password" class="form-control login-input" id="password" name="password" placeholder="รหัสผ่าน" value="">
                        <div class="input-group-prepend">
                          <div class="input-group-text login-input-group-text" id="btnGroupAddon"><span class="fa fa-lock"></span>
                          </div>
                        </div>
                      </div>
                      <span id="error-password" class="text-danger small"></span>
                    </div>
                    <div class="form-group">
                      <div class="custom-control custom-checkbox my-1 mr-sm-2">
                        <input type="checkbox" name="remember" value="1" class="custom-control-input" id="remember">
                        <label class="custom-control-label small" for="remember">จดจำฉัน</label>
                      </div>
                    </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary btn-block btn-login">เข้าสู่ระบบ</button>
                      <button class="btn btn-primary btn-block d-none btn-loading" type="button" disabled>
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        เข้าสู่ระบบ...
                      </button>
                    </div>
                    <div class="text-center">
                      <a href="#" class="small">ลืมรหัสผ่าน?</a>
                    </div>
                  </form>
                </div>
                <!-- สมัครสมาชิก Register -->
                <div class="tab-pane fade" id="pills-register" role="tabpanel" aria-labelledby="pills-register-tab">
                  <form id="frmRegister" name="frmRegister" action="#" method="POST" autocomplete="off">
                    <div class="alert alert-danger alert-incorrect d-none" role="alert"></div>

             
                    <div class="form-group regUsername">
                      <div class="input-group">
                        <input type="text" class="form-control login-input" id="regUsername" name="regUsername" placeholder="ชื่อผู้ใช้" value="">
                        <div class="input-group-prepend">
                          <div class="input-group-text login-input-group-text" id="btnGroupAddon"><span class="fa fa-user"></span>
                          </div>
                        </div>
                      </div>
                      <span id="error-regUsername" class="text-danger small"></span>
                    </div>
                    <div class="form-group regPassword">
                      <div class="input-group">
                        <input type="password" class="form-control login-input" id="regPassword" name="regPassword" placeholder="รหัสผ่าน" value="">
                        <div class="input-group-prepend">
                          <div class="input-group-text login-input-group-text" id="btnGroupAddon"><span class="fa fa-lock"></span>
                          </div>
                        </div>
                      </div>
                      <span id="error-regPassword" class="text-danger small"></span>
                    </div>
                    <div class="form-group regAgainPassworad">
                      <div class="input-group">
                        <input type="password" class="form-control login-input" id="regAgainPassworad" name="regAgainPassworad" placeholder="ยืนยันรหัสผ่าน" value="">
                        <div class="input-group-prepend">
                          <div class="input-group-text login-input-group-text" id="btnGroupAddon"><span class="fa fa-lock"></span>
                          </div>
                        </div>
                      </div>
                      <span id="error-regAgainPassworad" class="text-danger small"></span>
                    </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary btn-block btn-register">สมัครสมาชิก</button>
                      <button class="btn btn-primary btn-block d-none btn-loading" type="button" disabled>
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        สมัครสมาชิก...
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- EndBody -->


  <div id="deleteModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Delete ? <span class="account_id"></span></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body text-center">
          <h1 style="font-size:5.5rem;"><i class="fa fa-exclamation-circle text-danger" aria-hidden="true"></i></h1>
          <p>Are you sure you want to delete?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <a href="#" class="btn btn-danger btn-delete">Delete</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Footer -->
  <footer class="pt-5 pb-4" id="contact">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6 mt-2 mb-4">
          <h5 class="mb-4 font-weight-bold">เกี่ยวกับเรา</h5>
          <!-- <p class="mb-4"></p> -->
          <ul class="f-address">
            <li>
              <div class="row">
                <div class="col-1"><i class="fas fa-map-marker"></i></div>
                <div class="col-10">
                  <h6 class="font-weight-bold mb-0">ที่อยู่:</h6>
                  <p>123/456 หมู่ 1 บ้านสังแก ตำบลสะเดา อำเภอบัวเชด จังหวัดสุรินทร์ 32230</p>
                </div>
              </div>
            </li>
            <li>
              <div class="row">
                <div class="col-1"><i class="far fa-envelope"></i></div>
                <div class="col-10">
                  <h6 class="font-weight-bold mb-0">อีเมล์:</h6>
                  <p><a href="#">Devmour@devnokban.com</a></p>
                </div>
              </div>
            </li>
            <li>
              <div class="row">
                <div class="col-1"><i class="fas fa-phone-volume"></i></div>
                <div class="col-10">
                  <h6 class="font-weight-bold mb-0">โทร:</h6>
                  <p><a href="#">+66 (0) 89-123-1234</a></p>
                </div>
              </div>
            </li>
          </ul>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 mt-2 mb-4">
          <h5 class="mb-4 font-weight-bold">ข่าวสาร</h5>
          <ul class="f-address">
            <li>
              <div class="row">
                <div class="col-1"><i class="fab fa-twitter"></i></div>
                <div class="col-10">
                  <p class="mb-0"><a href="#">@devnokban </a> การสร้างเว็บไซต์</p>
                  <label>10 นาทีที่แล้ว</label>
                </div>
              </div>
            </li>
            <li>
              <div class="row">
                <div class="col-1"><i class="fab fa-twitter"></i></div>
                <div class="col-10">
                  <p class="mb-0"><a href="#">@devmour </a> การเชื่อมต่อฐานข้อมูล</p>
                  <label>20 นาทีที่แล้ว</label>
                </div>
              </div>
            </li>            
          </ul>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 mt-2 mb-4">
          <h5 class="mb-4 font-weight-bold">อัพเดทล่าสุด</h5>
          <ul class="recent-post">
            <li>
              <label class="mr-3">1 <br><span>พ.ย</span></label>
              <span>เข้าสู่ระบบ สมัครสมาชิก</span>
            </li>
            <!-- <li>
              <label class="mr-3">29 <br><span>APR</span></label>
              <span>Rendomised words which dont look eveable.</span>
            </li>
            <li>
              <label class="mr-3">30 <br><span>APR</span></label>
              <span>Rendomised words which dont look eveable.</span>
            </li> -->
          </ul>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 mt-2 mb-4">
          <h5 class="mb-4 font-weight-bold">เชื่อมต่อกับเรา</h5>
          <!-- <div class="input-group">
            <input type="text" class="form-control" placeholder="Your Email Address">
            <span class="input-group-addon" id="basic-addon2"><i class="fas fa-check"></i></span>
          </div> -->
          <ul class="social-pet mt-4">
            <li><a href="#" title="facebook"><i class="fab fa-facebook"></i></a></li>
            <li><a href="#" title="twitter"><i class="fab fa-twitter"></i></a></li>
            <li><a href="#" title="google-plus"><i class="fab fa-google-plus-g"></i></a></li>
            <li><a href="#" title="instagram"><i class="fab fa-instagram"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </footer>
  <!-- Copyright -->
  <section class="copyright">
    <div class="container">
      <div class="row">
        <div class="col-md-12 ">
          <div class="text-center text-white">
            &copy; 2019 Your Company. All Rights Reserved.
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- EndFooter -->


  <script src="node_modules/jquery/dist/jquery.min.js"></script>
  <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
  <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="include/libs/js/login.js"></script>


  <script>
    //เปลียนคลาส change class
    $(window).on('resize', function() {
      if ($(window).width() < 1200) {
        $("#BodyLogin").removeClass("container");
        $("#BodyLogin").addClass("container-fuid");
      } else {
        $("#BodyLogin").removeClass("container-fuid");
        $("#BodyLogin").addClass("container");
      }
    });

    $(document).ready(function() {


    });
  </script>


</body>

</html>