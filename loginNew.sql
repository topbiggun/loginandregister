-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 02, 2019 at 11:37 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `login_system_login`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `first_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `first_name`, `last_name`, `username`, `password`, `img`) VALUES
(2, 'admin', 'admin', 'admin', '123456', 'https://banner2.cleanpng.com/20180210/tyw/kisspng-cartoon-person-illustration-vector-cartoon-material-business-people-5a7f2f7b66f271.9036291015182846674217.jpg'),
(3, 'admin2', 'admin2', 'admin2', '123456', 'https://banner2.cleanpng.com/20180210/tyw/kisspng-cartoon-person-illustration-vector-cartoon-material-business-people-5a7f2f7b66f271.9036291015182846674217.jpg'),
(4, 'admin3', 'admin3', 'admin3', '123456', 'https://banner2.cleanpng.com/20180210/tyw/kisspng-cartoon-person-illustration-vector-cartoon-material-business-people-5a7f2f7b66f271.9036291015182846674217.jpg'),
(5, 'admin4', 'admin4', 'admin4', '123456', 'https://banner2.cleanpng.com/20180210/tyw/kisspng-cartoon-person-illustration-vector-cartoon-material-business-people-5a7f2f7b66f271.9036291015182846674217.jpg'),
(6, 'admin5', 'admin5', 'admin5', '123456', 'https://banner2.cleanpng.com/20180210/tyw/kisspng-cartoon-person-illustration-vector-cartoon-material-business-people-5a7f2f7b66f271.9036291015182846674217.jpg'),
(7, 'admin6', 'admin6', 'admin6', '123456', 'https://www.pinclipart.com/picdir/middle/490-4901349_person-png-animated-cartoon-man-transparent-background-clipart.png'),
(8, 'admin7', 'admin7', 'admin7', '123456', 'https://www.pinclipart.com/picdir/middle/490-4901349_person-png-animated-cartoon-man-transparent-background-clipart.png'),
(9, 'admin8', 'admin8', 'admin8', '123456', 'https://www.pinclipart.com/picdir/middle/490-4901349_person-png-animated-cartoon-man-transparent-background-clipart.png'),
(10, '123456', '123456', '123456', '123456', 'https://www.pinclipart.com/picdir/middle/490-4901349_person-png-animated-cartoon-man-transparent-background-clipart.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
